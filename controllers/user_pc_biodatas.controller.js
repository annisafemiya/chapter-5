const { user_pc_biodatas } = require('../models')

class BiodataController {
    static listBiodata(req, res) {
        user_pc_biodatas.findAll().then((data) => { res.status(200).json(data) })
            .catch((error) => { res.status(500).json(error) })
    }
}
module.exports = BiodataController
