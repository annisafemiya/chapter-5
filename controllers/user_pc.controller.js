const { user_pc } = require('../models')

class UserController {
    static listUser(req, res) {
        user_pc.findAll({
            attributes: ['id', 'username'],
        }).then((data) => { res.status(200).json(data) })
            .catch((error) => { res.status(500).json(error) })
    }

    static getByIdUser(req, res) {
        user_pc.findOne({
            where: {
                id: req.params.id
            }
        }).then((data) => { res.status(200).json(data) })
            .catch((err) => { res.status(500).json(err) })
    }

    static createUser(req, res) {
        user_pc.create({
            username: req.body.username,
            password: req.body.password,
        }).then((data) => { res.status(200).json(data) })
            .catch((err) => { res.status(500).json(err) })
    }

    static updateUser(req, res) {
        user_pc.update({
            username: req.body.username,
            password: req.body.password,
        }, {
            where: {
                id: req.params.id
            },
            returning: true,
        }).then((data) => { res.status(200).json(data[1][0]) })
            .catch((error) => { res.status(500).json(error) })
    }

    static deleteUser(req, res) {
        user_pc.destroy({
            where: {
                id: req.params.id
            }
        }).then((data) => { res.status(200).json({ message: 'Succesfully delete data' }) })
            .catch((error) => { res.status(500).json(error) })
    }
}
module.exports = UserController
