const express = require('express')
const router = express.Router()
const userGames = require('../routes/user_pc.route')

router.use('/userpc', userpc)

module.exports = router
