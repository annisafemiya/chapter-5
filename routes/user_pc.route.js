const express = require('express')
const router = express.Router()
const userpc = require('../routes/user_pc.route')
const BiodataController = require('../controllers/user_pc_biodatas.controller')
const HistoryController = require('../controllers/user_pc_histories.controller')


router.use(express.urlencoded({ extended: false }))
router.use(express.json())

// List
router.get('/', userpc.listUser)
router.get('/biodata', BiodataController.listBiodata)
router.get('/history', HistoryController.listHistory)


// CRUD user_games
router.post('/', userpc.createUser)
router.get('/:id', userpc.getByIdUser)
router.put('/:id', userpc.updateUser)
router.delete('/:id', userpc.deleteUser)

module.exports = router;
