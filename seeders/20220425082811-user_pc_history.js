'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('user_pc_histories', [
     {
       user_game_id: 1,
       entry_time: "01:00:30",
       time: 30,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       user_game_id: 2,
       entry_time: "11:30:44",
       time: 60,
       createdAt: new Date(),
       updatedAt: new Date()
     }
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_pc_histories', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
