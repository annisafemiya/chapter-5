'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('user_pc_biodatas', [
     {
       name: "annisafemiya",
       user_game_id: 1,
       gender: "Female",
       date_of_birth: "2001-07-28",
       age: 21,
       createdAt: new Date(),
       updatedAt: new Date()
     },
     {
       name: "John Doe",
       user_game_id: 2,
       gender: "Male",
       date_of_birth: "2001-11-22",
       age: 21,
       createdAt: new Date(),
       updatedAt: new Date()
     }
   ]);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_pc_biodatas', null, { truncate: true, restartIdentity: true });
  }
};
