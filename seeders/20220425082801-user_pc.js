'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('user_pc', [
      {
        username: "annisafemiya",
        password: "123456",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: "john doe",
        password: "654321",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_pc', null, { truncate: true, restartIdentity: true });
  }
};
