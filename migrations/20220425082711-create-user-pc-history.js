'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('user_pc_histories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_pc_id: {
        type: Sequelize.INTEGER,
        // references:{
        //   model: 'user_games',
        //   key: 'id'
        // },
        // onDelete: 'CASCADE'
      },
      entry_time: {
        type: Sequelize.TIME
      },
      time: {
        type: Sequelize.BIGINT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('user_pc_histories');
  }
};
